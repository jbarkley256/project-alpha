from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from tasks.models import Task
from tasks.forms import TaskForm


@login_required(redirect_field_name="login")
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = TaskForm()
    context = {
        "form": form,
    }
    return render(request, "tasks/create.html", context)


@login_required(redirect_field_name="login")
def tasks_list(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {
        "task_object": tasks,
    }
    return render(request, "tasks/mine.html", context)
